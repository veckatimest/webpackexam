var path = require('path')
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const ClosurePlugin = require('closure-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
	entry: {
		index: './src/index.js',
		another: './src/another-module.js',
	},
	output: {
		filename: '[name].bundle.js', // устанавливаем имя файла сборки
		path: path.resolve(__dirname, './dist'), // устанавливаем путь к сборке
		chunkFilename: '[name].bundle.js',
		publicPath: '/', // устанавливаем публичный путь, по которому файлы будут доступны
	},
	mode: 'production',
	module: {
		rules: [{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['babel-preset-env'],
						plugins: ['babel-plugin-transform-runtime']
					}
				}
			}, {
				test: /\.css$/,
				use: [
					{
						loader: MiniCssExtractPlugin.loader,
					},
					'css-loader'
				]
			}, {
				test: /\.(png|svg|jpg|gif)$/,
				use: [
				'file-loader'
				]
			}],
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: 'style.css',
		}),
		new HTMLWebpackPlugin({
			title: 'Code Splitting'
		}),
		new CleanWebpackPlugin(),
	],
	optimization: {
		minimizer: [
			new ClosurePlugin({ mode: 'STANDARD' }),
		],
		splitChunks: {
			cacheGroups: {
				commons: {
					name: 'commons',
					chunks: 'initial',
					minChunks: 1,
				}
			}
		}
	},
	devServer: {
		contentBase: path.join(__dirname, 'dist'),
		historyApiFallback: true,
		port: 9090,
		noInfo: true,
		overlay: true,
		publicPath: '/',
	},
}
