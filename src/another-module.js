import _ from 'lodash';
//import Promise from 'es6-promise'

console.log(
	_.join(['Another', 'module', 'loaded!'], ' ')
);

let a = new Promise((resolve, reject) => {
	setTimeout(() => {
		resolve("Hello from another-module");
	}, 100);
});

a.then((result) => {
	console.log(result);
});
